var i=0;
var total=0;
var prom=0;
var mayor=0;
var menor=0;

function calcular(){
    var valor=parseFloat(document.getElementById("valor").value);

    if(isNaN(valor)){
        alert("Debe ingresar un valor");
        return false;   
    }
    
    mayor=parseFloat(document.getElementById("mayor").value);
    menor=parseFloat(document.getElementById("menor").value);

    i++;

    if(mayor==0 && menor==0){
        mayor=valor;
        menor=valor;
    }else{
        if(valor>=mayor){
            mayor=valor;
        }else{
            menor=valor;
        }
    }

    total+=parseFloat(valor);
    prom=total/i;

    salida();
}

function salida(){
    document.getElementById("cantidad").value=i;
    document.getElementById("promedio").value=prom;
    document.getElementById("mayor").value=mayor;
    document.getElementById("menor").value=menor;
}

function resetea(){
    i=0;
    total=0;
    prom=0;
    mayor=0;
    menor=0;

    document.getElementById("valor").value="";
    document.getElementById("cantidad").value=0;
    document.getElementById("promedio").value=0;
    document.getElementById("mayor").value=0;
    document.getElementById("menor").value=0;
}